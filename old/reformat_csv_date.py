import os
from dataclasses import dataclass
import argparse
import csv
from typing import Any
from enum import Enum
from tqdm import tqdm


@dataclass
class Config:
    filepath: str
    positions: list[int]
    format: str
    delimiter: str


@dataclass
class CSVCache:
    elements: list[list[Any]]
    headers: list[Any]


class Formats(Enum):
    A = "M/D/Y"
    B = "D.M.Y"


def process_date_cell(date: str, date_format: str) -> str | None:
    """
    Convert cell to DataGrip Date Format: Y-M-D
    """
    if date == "NULL" or date is None or date == "" or date == "-":
        return None
    date = date.replace(" ", "")
    if date_format == Formats.A.name:
        split = date.split("/")
        if len(split) == 3:
            return f"{split[2]}-{split[0]}-{split[1]}"
    if date_format == Formats.B.name:
        split = date.split(".")
        if len(split) == 3:
            return f"{split[2]}-{split[1]}-{split[0]}"


def process_csv_file(filepath: str, positions: list[int], date_format: str, delimiter: str = ",") -> CSVCache | None:
    print(f'Start with file: {filepath}')
    cache = CSVCache(elements=[], headers=[])
    with open(filepath) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        first_row = True
        for row in tqdm(csv_reader):
            if first_row:
                first_row = False
                cache.headers = row.copy()
                continue
            new_row = row.copy()
            for position in positions:
                if position > len(row):
                    print(f"Defined position is greater than CSV row length")
                    return
                new_row[position] = process_date_cell(new_row[position], date_format)
            cache.elements.append(new_row)
    return cache


def write_processed_csv_file(cache: CSVCache, filepath: str, delimiter: str) -> None:
    new_filepath = os.path.splitext(filepath)[0]
    new_filepath += "_modified.csv"
    with open(new_filepath, mode="w") as csv_file:
        first_row = True
        csv_writer = csv.writer(csv_file, delimiter=delimiter)
        csv_writer.writerow(cache.headers)
        for row in cache.elements:
            csv_writer.writerow(row)


if __name__ == '__main__':
    """
    Reformat defined Data Cell into DataGrip CSV import format.
    Usage:
        python reformat_csv_date.py -f example.csv -p 3 -r A -d ,
    """
    parser = argparse.ArgumentParser(
        description="Parse CSV file dates",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-f", "--filepath", type=str, help="full path to file", required=True)
    parser.add_argument("-p", "--positions", nargs="+", help="date column numbers (as list)", required=True)
    parser.add_argument("-r", "--format", type=str, help="date format", required=True)
    parser.add_argument("-d", "--delimiter", type=str, help="CSV delimiter, default ','")

    args = parser.parse_args()

    config = Config(**vars(args))
    config.delimiter = "," if config.delimiter is None else config.delimiter

    empty_parameters_defined = config.filepath is None or config.format is None or config.positions is None
    null_position_defined = len(config.positions) == 0
    format_not_defined_correctly = config.format not in [e.name for e in Formats]

    print('settings.positions', config.positions)

    int_positions = [int(p) for p in config.positions if p.isdigit()]
    config.positions = int_positions

    if empty_parameters_defined:
        print("Filepath, Format or Date column position variables is empty")
        print(f"filename: {config.filepath}, "
              f"date column position: {config.positions}, "
              f"format: {config.format}, "
              f"delimiter: {config.delimiter}")
    elif null_position_defined:
        print("Date column position if not defined")
    elif format_not_defined_correctly:
        print(f"Date format defined incorrectly, use one of:")
        for f in [(e.name, e.value) for e in Formats]:
            print(f"Code {f[0]} means {f[1]}")
    else:
        csv_cache = process_csv_file(
            filepath=config.filepath,
            positions=config.positions,
            date_format=config.format,
            delimiter=config.delimiter
        )
        if csv_cache is None:
            print("Something went wrong")
        else:
            write_processed_csv_file(cache=csv_cache, filepath=config.filepath, delimiter=config.delimiter)
            print("Complete")
    
    # EXAMPLE: python reformat_csv_date.py -f s02_02.csv -p 1 5 7 9 11 14 -r B -d ,  
