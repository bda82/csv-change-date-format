import argparse
from dataclasses import dataclass
import csv
from typing import Any
import json
from tqdm import tqdm
from cast_rus_to_eng_symbols import match_rus, cast_rus_to_eng_symbols


CAST_RUS_TO_ENG_SYMBOLS = "CAST_RUS_TO_ENG_SYMBOLS"


@dataclass
class Config:
    filepath: str
    output: str
    template: str


@dataclass
class CSVElement:
    name: str
    value: Any


@dataclass
class CSVFile:
    headers: list[str]
    rows: list[list[CSVElement]]


@dataclass
class TemplateElement:
    name: str
    type: str
    null_value: Any
    special_cases: list


@dataclass
class Template:
    result_name: str
    template: list[TemplateElement]


def load_input_file(filepath: str) -> CSVFile:
    """
    Load input CSV file as list of rows
    """
    csv_file_content = CSVFile(headers=[], rows=[])
    first_row = True
    with open(filepath) as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            if first_row:
                first_row = False
                csv_file_content.headers = row
                continue
            row_template = []
            for r_index in range(len(row)):
                csv_element = CSVElement(name=csv_file_content.headers[r_index], value=row[r_index])
                row_template.append(csv_element)
            csv_file_content.rows.append(row_template)
    return csv_file_content


def load_template_file(template_file: str) -> Template:
    """
    Load template file JSON to Template формат
    """
    with open(template_file, "r") as json_file:
        json_content = json.load(json_file)
        
        result_name = json_content["result_name"]
        template_list = json_content["template"]

        template = Template(result_name=result_name, template=[])
        for template_list_element in template_list:
            element = TemplateElement(
                name=template_list_element["name"],
                type=template_list_element["type"],
                null_value=template_list_element["null_value"],
                special_cases=template_list_element.get("special_cases", [])
            )
            template.template.append(element)

        return template


def replace_quotations(value: str) -> str:
    result = value.replace('"', '')
    result = result.replace('""', '')
    result = result.replace('"""', '')
    return result


def create_result_file(input_file: CSVFile, template_file: Template, output_filename: str):
    """
    Create python "results" file with results dict
    """
    result_file = ""
    template_list = template_file.template

    for template in template_list:
        if template.type == "decimal":
            result_file += "from decimal import Decimal\n"
            break

    for template in template_list:
        if template.type == "datetime":
            result_file += "import datetime\n"
            break

    result_file += "\n\n"
    result_file += f"{template_file.result_name} = [\n"
    result_file += "    [\n"

    for csv_row in tqdm(input_file.rows):
        result_file += "        {\n"

        for csv_row_element in csv_row:
            template_element = None
            for template in template_list:
                if csv_row_element.name == template.name:
                    template_element = template
                    break

            if template_element is None:
                continue

            if template_element.type == "str":
                if csv_row_element.value is None or csv_row_element.value == "NULL" or csv_row_element.value == "":
                    result_file += f'            "{csv_row_element.name}": {template_element.null_value},\n'
                else:
                    if template_element.special_cases:
                        if CAST_RUS_TO_ENG_SYMBOLS in template_element.special_cases:
                            if match_rus(csv_row_element.value):
                                csv_row_element.value = cast_rus_to_eng_symbols(csv_row_element.value)
                    csv_row_element.value = replace_quotations(csv_row_element.value)
                    result_file += f'            "{csv_row_element.name}": "{csv_row_element.value}",\n'
            if template_element.type == "number" or template_element.type == "int":
                result_file += f'            "{csv_row_element.name}": {csv_row_element.value},\n'
            if template_element.type == "datetime":
                if csv_row_element.value == "" or csv_row_element.value is None or csv_row_element.value == "NULL" or csv_row_element.value == "-":
                    result_file += f'            "{csv_row_element.name}": {template_element.null_value},\n'
                    continue
                split = csv_row_element.value.split("-")
                value = f"{int(split[0])}, {int(split[1])}, {int(split[2])}"
                result_file += f'            "{csv_row_element.name}": datetime.date({value}),\n'
            if template_element.type == "decimal":
                if csv_row_element.value is None or csv_row_element.value == "":
                    result_file += f'            "{csv_row_element.name}": {template_element.null_value},\n'
                else:
                    result_file += f'            "{csv_row_element.name}": Decimal("{csv_row_element.value}"),\n'

        result_file += "        },\n"

    result_file += "    ]\n]"

    with open(output_filename, "w") as f:
        f.write(result_file)


if __name__ == '__main__':
    """
    Convert CSV file into results file
    Usage:
        python csv_to_results.py -f test.csv -o test.py -t test.json
    """
    parser = argparse.ArgumentParser(
        description="Parse CSV file dates",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-f", "--filepath", type=str, help="full path to file")
    parser.add_argument("-o", "--output", type=str, help="output file")
    parser.add_argument("-t", "--template", type=str, help="template file")

    args = parser.parse_args()
    config = Config(**vars(args))

    input_file = load_input_file(config.filepath)
    template_file = load_template_file(config.template)
    
    create_result_file(input_file=input_file, template_file=template_file, output_filename=config.output)
    
    print("Complete...")

    # EXAMPLE: python csv_to_results.py -f zs01_03.csv -o results.py -t zs01_03.csvresults.json 
