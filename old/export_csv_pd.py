import os
import pandas as pd

import argparse

parser = argparse.ArgumentParser(
    description="XLSX to csv",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("-f", "--filepath", type=str, help="full path to file")

args = parser.parse_args()
config = vars(args)

read_file = pd.read_excel(config["filepath"])

csv_filepath = os.path.splitext(config["filepath"])[0]
csv_filepath = csv_filepath + ".csv"

read_file.to_csv(csv_filepath, index = None, header=True)
