from typing import Generator

from sqlalchemy.engine import Connection, Engine


def connection(engine: Engine) -> Generator[Connection, None, None]:
    with engine.connect() as conn:
        yield conn
