from typing import Generator

from sqlalchemy import Connection, Engine


def transaction(engine: Engine) -> Generator[Connection, None, None]:
    with engine.connect() as conn:
        with conn.begin() as t:
            yield conn
            t.rollback()
