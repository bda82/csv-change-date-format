from typing import Generator

from sqlalchemy.engine import Engine

from old.common.dbflow_config import engine_
from old.common.metadata import metadata


def engine() -> Generator[Engine, None, None]:
    print("Create engine")
    yield engine_
    metadata.drop_all(engine_)
    engine_.dispose()
