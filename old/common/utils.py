import os
from csv import DictReader
from typing import Any, Generator

from old.common.dbflow_config import config


def build_path(base: str, folder: str, *fragments: str) -> str:
    return os.path.join(base, folder, *fragments)


def _sanitize(s: str) -> str | None:
    return s.strip("\ufeff") if s else None


def process_csv(
    file_path: str, extra_params: dict[str, Any] | None = None, delimiter: str = ","
) -> Generator[list[dict[str, Any]], None, None]:
    extra_params = extra_params or {}
    acc = 0
    batch: list[dict[str, Any]] = []
    with open(file_path, "r", encoding="utf-8") as f:
        reader = DictReader(f, delimiter=delimiter)
        for row in reader:
            acc += 1
            try:
                batch.append(
                    {
                        _sanitize(k): _sanitize(v) 
                        for k, v in row.items() 
                        if k is not None
                    } | extra_params
                )
            except Exception:
                print(row)
                raise
            if acc == config.BATCH_SIZE:
                yield batch
                acc = 0
                batch = []
        if batch:
            yield batch
