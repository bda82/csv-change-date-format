import os

from dotenv import load_dotenv
from jinja2 import Environment, FileSystemLoader
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine


class DBFlowConfig:
    BATCH_SIZE = 20000

    def __init__(self) -> None:
        load_dotenv()
        self.pg_url: str | None = os.getenv("PG_URL")
        self.pg_port: str | None = os.getenv("PG_PORT")
        self.pg_user: str | None = os.getenv("PG_USER")
        self.pg_pswd: str | None = os.getenv("PG_PSWD")
        self.pg_db: str | None = os.getenv("PG_DB", "dataflow")
        self.is_debug: bool = bool(int(os.getenv("DEBUG", 0)))
        self.sql_path: str = os.getenv("SQL_ROOT_PATH", "dags")
        self.pool_size: int = int(os.getenv("POOL_SIZE", 5))
        self.flow_data_path: str = os.getenv("FLOW_DATA_PATH", "flow_data")

    @property
    def postgesql_dsn(self) -> str:
        return f"postgresql+psycopg2://{self.pg_url}:{self.pg_port}/{self.pg_db}?user={self.pg_user}&password={self.pg_pswd}"


template: Environment = Environment(loader=FileSystemLoader(os.getcwd()))
config: DBFlowConfig = DBFlowConfig()
engine_: Engine = create_engine(url=config.postgesql_dsn, echo_pool=config.is_debug, pool_size=config.pool_size)
