from sqlalchemy import Column, Date, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_68 = Table(
    "s01_68",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("order", String),
    Column("inv_num", String),
    Column("object_name", String),
    Column("data_implementation", Date),
    Column("revenues_from_sales", Numeric(17, 2)),
    Column("initial_cost", Numeric(17, 2)),
    Column("amount_of_accrued_epreciation", Numeric(17, 2)),
    Column("residual_value", Numeric(17, 2)),
    Column("profit", Numeric(17, 2)),
    Column("lesion", Numeric(17, 2)),
    Column("entry_date", Date),
    Column("inn", String),
    Column("buyer_name", String),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
