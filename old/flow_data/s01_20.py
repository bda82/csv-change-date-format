from sqlalchemy import Column, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_20 = Table(
    "s01_20",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("account", String),
    Column("subaccount", String),
    Column("NREC(KAU1)", String),
    Column("name NREC(KAU1)", String),
    Column("bal_in_debet", Numeric(17, 2)),
    Column("bal_ref_debet", Numeric(17, 2)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
