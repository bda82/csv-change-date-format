from sqlalchemy import Column, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_72 = Table(
    "s01_72",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("account", String),
    Column("subaccount", String),
    Column("nrec(KAU4)", String),
    Column("name_(KAU4)", String),
    Column("nrec(KAU5)", String),
    Column("name_(KAU5)", String),
    Column("turn_dt", Numeric(17, 2)),
    Column("turn_kt", Numeric(17, 2)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
