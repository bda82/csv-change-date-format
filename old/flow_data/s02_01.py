from sqlalchemy import Column, Date, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s02_01 = Table(
    "s02_01",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("supplementary_sheet", Numeric(3)),
    Column("supp_sheet_comp", Date),
    Column("seq_num", Numeric(12)),
    Column("operation_type_code", String),
    Column("sell_invoice_num", String),
    Column("sell_invoice_date", Date),
    Column("product_type_code", String),
    Column("vendor_invoice_correc_num", String),
    Column("date_corr_seller_invoice", Date),
    Column("seller_invoice_num", String),
    Column("sales_invoice_date", Date),
    Column("corr_invoice_num", Numeric(3)),
    Column("corr_invoice_date", Date),
    Column("num_doc_confirm_payment", String),
    Column("document_date", String),
    Column("name_currency", String),
    Column("currency_code", String),
    Column("sales_value_in_currency", Numeric(19, 2)),
    Column("sales_value_in_rubles", Numeric(19, 2)),
    Column("cost_sales_rate_20", Numeric(19, 2)),
    Column("cost_sales_rate_18", Numeric(19, 2)),
    Column("cost_sales_rate_10", Numeric(19, 2)),
    Column("cost_sales_rate_0", Numeric(19, 2)),
    Column("vat_amount_rate_20", Numeric(19, 2)),
    Column("vat_amount_rate_18", Numeric(19, 2)),
    Column("vat_amount_rate_10", Numeric(19, 2)),
    Column("sales_value_exempt_from_tax", Numeric(19, 2)),
    Column("registration_num", String),
    Column("quant_unit_code", String),
    Column("quant_goods_subj_to_traceability", Numeric(10)),
    Column("cost_goods_subj_traceability", Numeric(17)),
    Column("id_buyer", String),
    Column("id_intermediary", String),
    Column("tipusers", String),
    Column("tipusers_1", String),
    Column("nrec", String),
    Column("export", String),
    Column("operation_r2", String),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", String),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
