from sqlalchemy import Column, Date, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_70 = Table(
    "s01_70",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("inv_num", String),
    Column("object_name", String),
    Column("entry_date", Date),
    Column("spi", Numeric(4)),
    Column("term_exploitation", Numeric(4)),
    Column("remaining_term", Numeric(4)),
    Column("loss_write_off_period", Numeric(4)),
    Column("loss_period_left", Numeric(4)),
    Column("lesion", Numeric(17, 2)),
    Column("loss_sum_monthlly", Numeric(17, 2)),
    Column("loss_written_off", Numeric(17, 2)),
    Column("tax_period_write_off", Numeric(17, 2)),
    Column("remaining_loss", Numeric(17, 2)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
