from sqlalchemy import Column, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_62 = Table(
    "s01_62",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("whole_total", Numeric(17, 2)),
    Column("subdivision_total", Numeric(17, 2)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
