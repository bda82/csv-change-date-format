from sqlalchemy import Column, Date, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_65 = Table(
    "s01_65",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("order", String),
    Column("inv_num", String),
    Column("object_name", String),
    Column("entry_date", Date),
    Column("spi", Numeric(4)),
    Column("initial_cost", Numeric(17, 2)),
    Column("modern_sum", Numeric(17, 2)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
