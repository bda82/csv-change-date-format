from sqlalchemy import Column, Date, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_64 = Table(
    "s01_64",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("order", String),
    Column("oper_date", Date),
    Column("object", String),
    Column("invest_int", Numeric()),
    Column("sub", Numeric(17, 2)),
    Column("account", String),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
