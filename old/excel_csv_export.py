import argparse
import openpyxl
from dataclasses import dataclass
import csv
from typing import Any
import json
from datetime import datetime
from tqdm import tqdm
from cast_rus_to_eng_symbols import cast_rus_to_eng_symbols


SAVE_AS_IS = "SAVE_AS_IS"
TRIM_WHITESPACES_BEFORE = "TRIM_WHITESPACES_BEFORE"
TRIM_WHITESPACES_AFTER = "TRIM_WHITESPACES_AFTER"
REMOVE_DOUBLE_QUOTATION_MARKS = "REMOVE_DOUBLE_QUOTATION_MARKS"
REMOVE_COMMA_INSIDE_FIELD = "REMOVE_COMMA_INSIDE_FIELD"
CLIPPING_NUMBERS_AFTER_DECIMAL_POINT = "CLIPPING_NUMBERS_AFTER_DECIMAL_POINT"
REMOVE_NEW_LINE_SYMBOLS = "REMOVE_NEW_LINE_SYMBOLS"
CLIPPING_NUMBERS_AFTER_DECIMAL_POINT_COUNT = 2

# SPECIAL CASES
SPECIAL_PROCESS_AS_PERIOD = "SPECIAL_PROCESS_AS_PERIOD"
SPECIAL_DOES_NOT_APPEND_SYMBOL_M = "SPECIAL_DOES_NOT_APPEND_SYMBOL_M"
SPECIAL_RULES = [SPECIAL_PROCESS_AS_PERIOD]

CAST_RUS_TO_ENG_SYMBOLS = "CAST_RUS_TO_ENG_SYMBOLS"

DATATYPE_STRING = "string"
DATATYPE_DATETIME = "datetime"
DATATYPE_NUMBER = "number"
DATATYPE_INTEGER = "integer"


@dataclass
class Config:
    source: str
    output: str
    rules: str


@dataclass
class Rule:
    column: str
    datatype: str
    tokens: list[str]
    nullvalue: Any


@dataclass
class Rules:
    rules: list[Rule]
    delimiter: str
    headers: bool
    last_column: int = 0
    last_row: int = 0


@dataclass
class Cell:
    column: str
    value: Any


@dataclass
class CSVFile:
    headers: list[str]
    rows: list[Any]


@dataclass
class ExcelFile:
    headers: list[str]
    rows: list[list[Cell]]


def load_rules_file(filepath: str) -> Rules:
    with open(filepath, "r") as json_file:
        json_content = json.load(json_file)

        headers = json_content.get("headers", True)
        delimeter = json_content.get("delimiter", ",")
        rules = json_content.get("rules", [])
        last_column = json_content.get("last_column", 0)
        last_row = json_content.get("last_row", 0)

        rules_dc = []

        for rule in rules:
            column = rule.get("column")
            datatype = rule.get("datatype", "str")
            nullvalue = rule.get("nullvalue")
            tokens = rule.get("tokens", [])
            rule_dc = Rule(
                column=column,
                datatype=datatype,
                nullvalue=nullvalue,
                tokens=tokens
            )
            rules_dc.append(rule_dc)

        return Rules(
            headers=headers,
            delimiter=delimeter,
            rules=rules_dc,
            last_column=last_column,
            last_row=last_row
        )


def define_last_not_null_row(sheet) -> int:
    last_row = 0
    for row in tqdm(range(1, sheet.max_row+1)):
        cells = []
        for column in range(1, sheet.max_column+1):
            cell = sheet.cell(row=row, column=column)
            if cell:
                cells.append(True)
        if not any(cells):
            return last_row
        last_row += 1
    return last_row


def define_last_not_null_column(sheet) -> int:
    columns = 0
    for column in tqdm(range(1, sheet.max_column + 1)):
        cell = sheet.cell(row=1, column=column)
        if not cell:
            break
        else:
            columns += 1
    return columns


def load_excel_file(filepath: str, rules: Rules) -> ExcelFile:
    workbook = openpyxl.load_workbook(filepath)
    sheet = workbook.active

    first_row = True

    headers = []
    rows = []

    if rules.last_row == 0:
        print("Define last row number from Sheet...")
        last_row = define_last_not_null_row(sheet)
    else:
        last_row = rules.last_row
        print("Apply last row number from rules file...")
    
    if rules.last_column == 0:
        print("Define last column number from Sheet...")
        last_column = define_last_not_null_column(sheet)
    else:
        print("Apply last column number from Sheet...")
        last_column = rules.last_column

    print("Rows quantity: ", last_row)
    print("Columns quantity: ", last_column)

    for row in range(1, last_row+1):
        row_n: list[Cell] = []
        for column in range(1, last_column+1):
            cell = sheet.cell(row=row, column=column)
            if first_row:
                headers.append(cell.value)
            else:
                cell_dc = Cell(column=headers[column-1], value=cell.value)
                row_n.append(cell_dc)
        if first_row:
            first_row = False
        else:
            rows.append(row_n)

    return ExcelFile(headers=headers, rows=rows)


def datetime_to_csv_format(dt: datetime):
    year = str(dt.year)
    month = str(dt.month) if dt.month >= 10 else f"0{dt.month}"
    day = str(dt.day) if dt.day >= 10 else f"0{dt.day}"
    return f"{year}-{month}-{day}"


def find_rule(column: str, rules: Rules) -> Rule | None:
    for rule in rules.rules:
        if column == rule.column:
            return rule


def convert_row_into_csv(row: list[Cell], rules: Rules) -> list[Cell]:
    converted: list[Cell] = []
    for element in row:
        converted_element = element.value
        if converted_element is not None:
            rule = find_rule(element.column, rules)
            if rule:
                if SAVE_AS_IS in rule.tokens:
                    pass
                else:
                    special_tokens = []
                    for token in rule.tokens:
                        if token in SPECIAL_RULES:
                            special_tokens.append(token)
                    if special_tokens:
                        for special_token in special_tokens:
                            if special_token == SPECIAL_PROCESS_AS_PERIOD:
                                converted_element = str(converted_element)
                                last_letter = converted_element[-1]
                                if not last_letter.isdigit():
                                    last_letter = last_letter.replace("М", "M").replace("У", "Y")
                                else:
                                    last_letter = ""
                                digits = int(''.join(filter(str.isdigit, converted_element)))
                                if digits < 10:
                                    converted_element = f"0{digits}{last_letter}"
                                else:
                                    converted_element = f"{digits}{last_letter}"
                            if special_token == SPECIAL_DOES_NOT_APPEND_SYMBOL_M:
                                converted_element = converted_element.replace("M", "")
                    else:
                        if rule.datatype == DATATYPE_STRING:
                            for token in rule.tokens:
                                if token == CAST_RUS_TO_ENG_SYMBOLS:
                                    converted_element = cast_rus_to_eng_symbols(converted_element, rule.nullvalue)
                                if token == TRIM_WHITESPACES_AFTER:
                                    converted_element = converted_element.rstrip()
                                if token == TRIM_WHITESPACES_BEFORE:
                                    converted_element = converted_element.lstrip()
                                if token == REMOVE_COMMA_INSIDE_FIELD:
                                    converted_element = converted_element.replace(",", "")
                                if token == REMOVE_DOUBLE_QUOTATION_MARKS:
                                    converted_element = converted_element.replace('"', "")
                                if token == REMOVE_NEW_LINE_SYMBOLS:
                                    converted_element = converted_element.replace("\n", "")
                        if rule.datatype == DATATYPE_DATETIME:
                            if isinstance(converted_element, datetime):
                                converted_element = datetime_to_csv_format(converted_element)
                        if rule.datatype == DATATYPE_INTEGER:
                            converted_element = int(converted_element)
                        if rule.datatype == DATATYPE_NUMBER:
                            if CLIPPING_NUMBERS_AFTER_DECIMAL_POINT in rule.tokens:
                                fmt = "{:." + str(CLIPPING_NUMBERS_AFTER_DECIMAL_POINT_COUNT) + "f}"
                                converted_element = float(fmt.format(converted_element))
        converted.append(converted_element)
    return converted


def save_csv_file(excel_content: ExcelFile, filepath: str, rules: Rules):
    cache = []
    if rules.headers:
        cache.append(excel_content.headers)
    for row in tqdm(excel_content.rows):
        cache.append(convert_row_into_csv(row, rules))
    with open(filepath, "w") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=rules.delimiter)
        for row in cache:
            csv_writer.writerow(row)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Excel to CSV file",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-s", "--source", type=str, help="full path to excel file")
    parser.add_argument("-o", "--output", type=str, help="output file")
    parser.add_argument("-r", "--rules", type=str, help="rules file")

    args = parser.parse_args()
    config = Config(**vars(args))

    rules: Rules = load_rules_file(config.rules)

    excel_file_content: ExcelFile = load_excel_file(config.source, rules)

    save_csv_file(excel_file_content, config.output, rules)

    print("Complete...")

    # EXAMPLE: python excel_csv_export.py -s s02_02_vigruzka.xlsx -o s02_02.csv -r s02_02.excelcsv.json 
