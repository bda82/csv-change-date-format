import os
import importlib
from typing import Collection

import yaml
from dataclasses import dataclass
from dacite import from_dict
from dotenv import load_dotenv
from sqlalchemy import create_engine, text, Table

from old.tables import metadata

from old.common import process_csv, build_path

FILL_DATABASE = "FILL_DATABASE"
EXECUTE_SQL = "EXECUTE_SQL"


@dataclass
class Scenario:
    title: str
    drop_before: bool
    objects: list[str]
    flow_data_path: str


class Config:
    def __init__(self):
        load_dotenv()
        self.pg_url: str | None = os.getenv("PG_URL", "localhost")
        self.pg_port: str | None = os.getenv("PG_PORT", 5432)
        self.pg_user: str | None = os.getenv("PG_USER", "dataflow")
        self.pg_pswd: str | None = os.getenv("PG_PSWD", "password")
        self.pg_db: str | None = os.getenv("PG_DB", "dataflow")
        self.is_debug: bool = bool(int(os.getenv("DEBUG", 0)))
        self.pool_size: int = int(os.getenv("POOL_SIZE", 5))
        self.postgesql_dsn: str = f"postgresql+psycopg2://{self.pg_url}:{self.pg_port}/{self.pg_db}?user={self.pg_user}&password={self.pg_pswd}"
        self.flow_data_path: str = os.getenv("FLOW_DATA_PATH", "flow_data")


class Repository:
    config = Config()
    metadata = metadata

    def __init__(self):
        self.engine = create_engine(url=self.config.postgesql_dsn, echo_pool=self.config.is_debug, pool_size=self.config.pool_size)

    def execute_sql_query(self, query_string: str, sql_parameters: dict | None = None):
        query = text(query_string)
        with self.engine.connect() as connection:
            if sql_parameters is None:
                result = connection.execute(query)
            else:
                result = connection.execute(query, parameters=sql_parameters)
        try:
            return [r for r in result.all()]
        except Exception as ex:
            print(ex)
        return []

    def load_batch(self, schema: Table, batch):
        with self.engine.connect() as connection:
            with connection.begin() as transaction:
                transaction.connection.execute(schema.insert().values(batch))

    def create_schema(self, schema: Table):
        return schema.create(self.engine)

    def drop_schema(self, schema: Table):
        return schema.drop(self.engine)

    def read_schema(self, schema):
        query = f"select * from {schema.name}"
        return self.execute_sql_query(query)

    def dispose(self):
        self.metadata.drop_all(bind=self.engine)
        self.engine.dispose()


class Dataflow:
    config: Config = Config()
    repo: Repository = Repository()
    scenario_config: Scenario | None = None

    def __list_csv_files(self):
        lst = os.listdir(self.scenario_config.flow_data_path)
        return [f for f in lst if f.endswith(".csv")]

    def __list_sql_files(self):
        lst = os.listdir(self.scenario_config.flow_data_path)
        return [f for f in lst if f.endswith(".sql")]

    def __list_schema_files(self):
        lst = os.listdir(self.scenario_config.flow_data_path)
        return [f for f in lst if f.endswith(".py") and f != "__init__.py"]

    def __verify_csv_name(self, name: str) -> bool:
        return f"{name}.csv" in self.csv_list

    def __verify_sql_name(self, name: str) -> bool:
        return f"{name}.sql" in self.sql_list

    def __verify_schema_name(self, name: str) -> bool:
        return f"{name}.py" in self.schema_list

    def __define_csv_path_by_name(self, name: str) -> str:
        return build_path(self.scenario_config.flow_data_path, f"{name}.csv")

    def __define_sql_path_by_name(self, name: str) -> str:
        return build_path(self.scenario_config.flow_data_path, f"{name}.sql")

    def __define_schema_path_by_name(self, name: str) -> str:
        return build_path(self.scenario_config.flow_data_path, f"{name}.py")

    def __import_schema(self, name: str) -> Table:
        try:
            module_name = f"{self.scenario_config.flow_data_path}.{name}"
            module_ = importlib.import_module(module_name)
            return getattr(module_, name)
        except Exception as ex:
            print(ex)

    def __load_csv_file(self, name: str) -> None:
        path = self.__define_csv_path_by_name(name)
        schema = self.__import_schema(name)
        try:
            self.repo.create_schema(schema)
            print(f"Load batch from CSV into {name}")
            for batch in process_csv(path):
                self.repo.load_batch(schema, batch)
        except Exception as ex:
            print(ex)

    def __execute_sql_file(self, name) -> Collection:
        path = self.__define_sql_path_by_name(name)
        with open(path, "r") as f:
            query = f.read()
        return self.repo.execute_sql_query(query)

    def __scenario_fill_database(self):
        print(f"Run scenario: {self.scenario_config.title}")
        if self.scenario_config.drop_before:
            self.repo.dispose()
        for name in self.scenario_config.objects:
            if not self.__verify_schema_name(name) or not self.__verify_csv_name(name):
                print(f"Cant find object Schema or CSV for name {name}")
                continue
            print(f"Load CSV for name {name}...")
            self.__load_csv_file(name)
            print("Complete")

    def __scenario_execute_sqls(self):
        print(f"Run scenario: {self.scenario_config.title}")
        if self.scenario_config.drop_before:
            self.repo.dispose()
        for name in self.scenario_config.objects:
            if not self.__verify_schema_name(name) or not self.__verify_sql_name(name):
                print(f"Cant find object Schema or SQL for name {name}")
                continue
            print(f"Execute SQL for name {name}...")
            data = []
            try:
                data = self.__execute_sql_file(name)
            except Exception as ex:
                print(ex)
            print(f"Create Schema for SQL for name {name}...")
            schema = self.__import_schema(name)
            try:
                self.repo.create_schema(schema)
            except Exception as ex:
                print(ex)
            print(f"Insert {len(data)} elements into Schema name {name}...")
            for batch in data:
                self.repo.load_batch(schema, batch)

    def run_scenario(self, name: str, type_: str = FILL_DATABASE):
        scenario_config_dict = self.load_scenario_file(name)
        self.scenario_config = from_dict(data_class=Scenario, data=scenario_config_dict)
        self.csv_list = self.__list_csv_files()
        self.sql_list = self.__list_sql_files()
        self.schema_list = self.__list_schema_files()
        if type_ == FILL_DATABASE:
            return self.__scenario_fill_database()
        elif type_ == EXECUTE_SQL:
            return self.__scenario_execute_sqls()
        else:
            print(f"No algorithm found for scenario {type_}")

    @staticmethod
    def load_scenario_file(name: str) -> dict:
        with open(name, "r") as y:
            data = yaml.load(y, Loader=yaml.FullLoader)
        return data


d = Dataflow()
d.run_scenario(name="default_scenario.yaml", type_=FILL_DATABASE)
d.run_scenario(name="fill_db_scenario.yaml", type_=FILL_DATABASE)
d.run_scenario(name="execute_sql_scenario.yaml", type_=EXECUTE_SQL)
