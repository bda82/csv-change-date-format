from dataclasses import dataclass


@dataclass
class Config:
    function: str
    schema: str
    input: str
    output: str
