from dataclasses import dataclass

from src.tools.reformat_csv_date.components.position import Position


@dataclass
class LocalConfig:
    positions: list[Position]
    delimiter: str
