from enum import Enum


class Formats(Enum):
    A = "M/D/Y"
    B = "D.M.Y"
