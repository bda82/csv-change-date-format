from dataclasses import dataclass


@dataclass
class Position:
    column: int
    format: str
