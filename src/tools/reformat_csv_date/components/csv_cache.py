from dataclasses import dataclass
from typing import Any


@dataclass
class CSVCache:
    elements: list[list[Any]]
    headers: list[Any]