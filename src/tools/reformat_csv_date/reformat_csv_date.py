import os
import csv
import yaml
from tqdm import tqdm

from src.tools.base_function import BaseFunction
from src.config import Config

from src.tools.reformat_csv_date.components.formats import Formats
from src.tools.reformat_csv_date.components.csv_cache import CSVCache
from src.tools.reformat_csv_date.components.local_config import LocalConfig
from src.tools.reformat_csv_date.components.position import Position


class ReformatDate(BaseFunction):
    def run(self, config: Config):
        """
        Example call:

        python dataflow.py -f reformat_csv_date -s s02_02_reformat_csv_date_schema.yaml -i s02_02.csv -o s02_02_correct_dates.csv

        Example YAML schema:

        delimiter: ,
        positions:
          -
            column: 1
            format: B
          -
            column: 5
            format: B
          -
            column: 7
            format: B
          -
            column: 9
            format: B
          -
            column: 11
            format: B
          -
            column: 14
            format: B


        @param config:
        @return:
        """
        local_config = self.__load_schema_file(config.schema)
        csv_cache = self.__process_csv_file(
            config.input,
            local_config
        )
        if csv_cache is None:
            print("Something went wrong")
        else:
            self.__write_processed_csv_file(
                cache=csv_cache,
                filename=config.output,
                delimiter=local_config.delimiter
            )
            print("Complete")

    def __load_schema_file(self, filename: str) -> LocalConfig:
        filepath: str = self.__join_schema_path(filename)
        with open(filepath, "r") as y:
            json_content = yaml.load(y, Loader=yaml.FullLoader)
        delimiter = json_content.get("delimiter", ",")
        positions = json_content.get("positions", [])
        positions_list = []
        for position in positions:
            pos: Position = Position(column=position.get("column"), format=position.get("format"))
            positions_list.append(pos)
        return LocalConfig(delimiter=delimiter, positions=positions_list)

    def __process_date_cell(self, date: str, date_format: str) -> str | None:
        """
        Convert cell to DataGrip Date Format: Y-M-D
        """
        if date == self.settings.tokens.NULL or date is None or date == self.settings.tokens.EMPTY or date == self.settings.tokens.EMPTY_DASH:
            return None

        date = date.replace(" ", "")

        if date_format == Formats.A.name:
            split = date.split("/")
            if len(split) == 3:
                return f"{split[2]}-{split[0]}-{split[1]}"

        if date_format == Formats.B.name:
            split = date.split(".")
            if len(split) == 3:
                return f"{split[2]}-{split[1]}-{split[0]}"

    def __join_csv_path(self, filename: str):
        return os.path.join(self.csv_folder, filename)

    def __join_schema_path(self, filename: str):
        return os.path.join(self.schemas_folder, filename)

    def __process_csv_file(self, filename: str, config_: LocalConfig) -> CSVCache | None:
        filepath = self.__join_csv_path(filename)
        print(f'Start with file: {filepath}')
        cache = CSVCache(elements=[], headers=[])
        with open(filepath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=config_.delimiter)
            first_row = True
            for row in tqdm(csv_reader):
                if first_row:
                    first_row = False
                    cache.headers = row.copy()
                    continue
                new_row = row.copy()
                for position in config_.positions:
                    if position.column > len(row):
                        print(f"Defined position is greater than CSV row length")
                        return
                    new_row[position.column] = self.__process_date_cell(
                        new_row[position.column],
                        position.format
                    )
                cache.elements.append(new_row)
        return cache

    def __write_processed_csv_file(self, cache: CSVCache, filename: str, delimiter: str) -> None:
        filepath = self.__join_csv_path(filename)
        with open(filepath, mode="w") as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=delimiter)
            csv_writer.writerow(cache.headers)
            for row in cache.elements:
                csv_writer.writerow(row)