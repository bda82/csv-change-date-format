import os
from typing import Collection

import yaml
import importlib
from sqlalchemy import Table

from dacite import from_dict

from src.tools.base_function import BaseFunction
from src.config import Config

from src.tools.db_flow.components.local_config import LocalConfig
from src.tools.db_flow.components.repository import Repository
from src.tools.db_flow.components.scenario_csv import ScenarioCSV
from src.tools.db_flow.components.scenario_sql import ScenarioSQL

from src.tools.db_flow.utils import build_path, process_csv


class DBFlow(BaseFunction):

    config: LocalConfig = LocalConfig()
    repo: Repository = Repository()
    scenario_config: ScenarioCSV | ScenarioSQL | None = None

    def run(self, config: Config):
        """
        Example call:

        python dataflow.py -f db_flow -s default_scenario.yaml

        Example YAML schema:

        title: Fill Database
        type: FILL_DATABASE
        flow_data_path: flow_data
        drop_before: true
        objects:
          - attempts

        @param config:
        @return:
        """
        scenario_config_dict = self.__load_scenario_file(config.schema)
        title_ = scenario_config_dict.get("title", "Default scenario")
        print(f"Run scenario: {title_}")
        type_ = scenario_config_dict.get("type", "FILL_DATABASE")
        if type_ == self.settings.tokens.FILL_DATABASE:
            self.scenario_config = from_dict(data_class=ScenarioCSV, data=scenario_config_dict)
            self.__prepare_scenario()
            return self.__scenario_fill_database()
        elif type_ == self.settings.tokens.EXECUTE_SQL:
            self.scenario_config = from_dict(data_class=ScenarioSQL, data=scenario_config_dict)
            for scenario in self.scenario_config.objects:
                scenario.__dict__.update({"registry_name": scenario.name, "execution_date": datetime.now()})  # noqa
            self.__prepare_scenario()
            return self.__scenario_execute_sqls()
        else:
            print(f"No algorithm found for scenario {type_}")

    def __list_csv_files(self):
        lst = os.listdir(self.scenario_config.flow_data_path)
        return [f for f in lst if f.endswith(".csv")]

    def __list_sql_files(self):
        lst = os.listdir(self.scenario_config.flow_data_path)
        return [f for f in lst if f.endswith(".sql")]

    def __list_schema_files(self):
        lst = os.listdir(self.scenario_config.flow_data_path)
        return [f for f in lst if f.endswith(".py") and f != "__init__.py"]

    def __verify_csv_name(self, name: str) -> bool:
        return f"{name}.csv" in self.csv_list

    def __verify_sql_name(self, name: str) -> bool:
        return f"{name}.sql" in self.sql_list

    def __verify_schema_name(self, name: str) -> bool:
        return f"{name}.py" in self.schema_list

    def __define_csv_path_by_name(self, name: str) -> str:
        return build_path(self.scenario_config.flow_data_path, f"{name}.csv")

    def __define_sql_path_by_name(self, name: str) -> str:
        return build_path(self.scenario_config.flow_data_path, f"{name}.sql")

    def __define_schema_path_by_name(self, name: str) -> str:
        return build_path(self.scenario_config.flow_data_path, f"{name}.py")

    def __import_schema(self, name: str) -> Table:
        try:
            module_name = f"{self.scenario_config.flow_data_path}.{name}"
            module_ = importlib.import_module(module_name)
            return getattr(module_, name)
        except Exception as ex:
            print(ex)

    def __load_csv_file(self, name: str) -> None:
        path = self.__define_csv_path_by_name(name)
        schema = self.__import_schema(name)
        try:
            self.repo.create_schema(schema)
            print(f"Load batch from CSV into {name}")
            for batch in process_csv(path):
                self.repo.load_batch(schema, batch)
        except Exception as ex:
            print(ex)

    def __execute_sql_file(self, name: str, parameters: dict | None) -> Collection:
        path = self.__define_sql_path_by_name(name)
        with open(path, "r") as f:
            query = f.read()
        if parameters is None:
            return self.repo.execute_sql_query(query)
        return self.repo.execute_sql_query(query, sql_parameters=parameters)

    def __scenario_fill_database(self):
        for name in self.scenario_config.objects:
            if not self.__verify_schema_name(name) or not self.__verify_csv_name(name):
                print(f"Cant find object Schema or CSV for name {name}")
                continue
            print(f"Load CSV for name {name}...")
            self.__load_csv_file(name)
            print("Complete")

    def __scenario_execute_sqls(self):
        for obj in self.scenario_config.objects:
            name = obj.name  # noqa
            if not self.__verify_schema_name(name) or not self.__verify_sql_name(name):
                print(f"Cant find object Schema or SQL for name {name}")
                continue

            print(f"Create Schema for SQL for name {name}...")
            schema = self.__import_schema(name)

            try:
                self.repo.create_schema(schema)
            except Exception as ex:
                print(ex)

            print(f"Execute SQL for name {name}...")

            data = []
            parameters = asdict(obj)  # noqa
            try:
                parameters.pop("id")
                parameters.pop("name")
                parameters.pop("number")
                parameters.pop("execution_date")
                data = self.__execute_sql_file(name, parameters)
            except Exception as ex:
                print(ex)

            print(f"Insert {len(data)} elements into Schema name {name}...")

            column_names = tuple(c.name for c in schema.c)  # noqa

            index = 1
            attempt_id = parameters.get("attempt_id", 1)

            tuple_data = [tuple(data_element) for data_element in data]

            extended_data = []
            for data_element in tuple_data:
                ud = (index, attempt_id) + data_element
                extended_data.append(ud)
                index += 1

            final_data_as_dicts = [dict(zip(column_names, x)) for x in extended_data]

            self.repo.load_batch(schema, final_data_as_dicts)

    def __load_scenario_file(self, name: str) -> dict:
        filepath = self.__join_schema_path(name)
        with open(filepath, "r") as y:
            data = yaml.load(y, Loader=yaml.FullLoader)
        return data

    def __join_schema_path(self, filename: str) -> str:
        return os.path.join(self.scenarios_folder, filename)

    def __prepare_scenario(self):
        if self.scenario_config.drop_before:
            self.repo.dispose()
        self.csv_list = self.__list_csv_files()
        self.sql_list = self.__list_sql_files()
        self.schema_list = self.__list_schema_files()
