from sqlalchemy import create_engine, text, Table

from src.tools.db_flow.components.metadata import metadata
from src.tools.db_flow.components.local_config import LocalConfig


class Repository:
    config = LocalConfig()
    metadata = metadata

    def __init__(self):
        self.engine = create_engine(url=self.config.postgesql_dsn, echo_pool=self.config.is_debug, pool_size=self.config.pool_size)

    def execute_sql_query(self, query_string: str, sql_parameters: dict | None = None):
        query = text(query_string)
        with self.engine.connect() as connection:
            if sql_parameters is None:
                result = connection.execute(query)
            else:
                result = connection.execute(query, parameters=sql_parameters)
        try:
            return [r for r in result.all()]
        except Exception as ex:
            print(ex)
        return []

    def load_batch(self, schema: Table, batch):
        with self.engine.connect() as connection:
            with connection.begin() as transaction:
                transaction.connection.execute(schema.insert().values(batch))

    def create_schema(self, schema: Table):
        return schema.create(self.engine)

    def drop_schema(self, schema: Table):
        return schema.drop(self.engine)

    def read_schema(self, schema):
        query = f"select * from {schema.name}"
        return self.execute_sql_query(query)

    def dispose(self):
        self.metadata.drop_all(bind=self.engine)
        self.engine.dispose()