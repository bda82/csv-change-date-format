from dataclasses import dataclass
from datetime import datetime


@dataclass
class ScenarioObject:
    name: str
    id: int
    number: int
    be: str
    year: int
    period: str
    scenario_id: int
    corr_num: int
    registry_name: str | None
    execution_date: datetime | None
