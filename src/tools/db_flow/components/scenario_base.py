from typing import Any
from dataclasses import dataclass


@dataclass
class ScenarioBase:
    title: str
    drop_before: bool
    flow_data_path: str
    objects: list[Any]