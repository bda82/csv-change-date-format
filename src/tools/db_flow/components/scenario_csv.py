from dataclasses import dataclass

from src.tools.db_flow.components.scenario_base import ScenarioBase


@dataclass
class ScenarioCSV(ScenarioBase):
    objects: list[str]
