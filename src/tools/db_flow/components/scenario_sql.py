from dataclasses import dataclass

from src.tools.db_flow.components.scenario_base import ScenarioBase
from src.tools.db_flow.components.scenario_object import ScenarioObject


@dataclass
class ScenarioSQL(ScenarioBase):
    objects: list[ScenarioObject]
