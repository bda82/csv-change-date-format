import os
from dotenv import load_dotenv


class LocalConfig:
    def __init__(self):
        load_dotenv()
        self.pg_url: str | None = os.getenv("PG_URL", "localhost")
        self.pg_port: str | None = os.getenv("PG_PORT", 5432)
        self.pg_user: str | None = os.getenv("PG_USER", "dataflow")
        self.pg_pswd: str | None = os.getenv("PG_PSWD", "password")
        self.pg_db: str | None = os.getenv("PG_DB", "dataflow")
        self.is_debug: bool = bool(int(os.getenv("DEBUG", 0)))
        self.pool_size: int = int(os.getenv("POOL_SIZE", 5))
        self.postgesql_dsn: str = f"postgresql+psycopg2://{self.pg_url}:{self.pg_port}/{self.pg_db}?user={self.pg_user}&password={self.pg_pswd}"
        self.flow_data_path: str = os.getenv("FLOW_DATA_PATH", "flow_data")
