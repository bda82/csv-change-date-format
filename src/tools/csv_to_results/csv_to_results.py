import os
import csv
import yaml
from tqdm import tqdm


from src.tools.base_function import BaseFunction
from src.config import Config

from src.tools.csv_to_results.components.csv_element import CSVElement
from src.tools.csv_to_results.components.csv_file import CSVFile
from src.tools.csv_to_results.components.temmplate_element import TemplateElement
from src.tools.csv_to_results.components.template import Template
from src.tools.csv_to_results.utils.cast_rus_to_eng_symbols import match_rus, cast_rus_to_eng_symbols


class CSVToResults(BaseFunction):

    def run(self, config: Config):
        """
        Example call:

        python dataflow.py -f csv_to_results -s zs01_03.csv -i zs01_03_csvresults_schema.yaml -o zs01_03.py

        Example YAML schema:

        result_name: EXPECTED_RESULT
        template:
          -
            name: account
            type: str
            null_value: null
          -
            name: subaccount
            type: str
            null_value: null
          -
            name: corr_acc
            type: str
            null_value: null
          -
            name: NREC(KAU1)
            type: str
            null_value: null
          -
            name: name_NREC(KAU1)
            type: str
            null_value: null
          -
            name: bal_in_debet
            type: decimal
            null_value: null
          -
            name: turn_dt
            type: decimal
            null_value: null
          -
            name: turn_kt
            type: decimal
            null_value: null
          -
            name: bal_ref_debet
            type: decimal
            null_value: null

        @param config:
        @return:
        """
        input_file = self.__load_input_file(config.input)
        schema_file = self.__load_schema_file(config.schema)
        self.__create_result_file(
            input_file=input_file,
            template_file=schema_file,
            output_filename=config.output
        )

    def __join_csv_path(self, filename: str):
        return os.path.join(self.csv_folder, filename)

    def __join_rules_path(self, filename: str):
        return os.path.join(self.schemas_folder, filename)

    def __join_results_path(self, filename: str):
        return os.path.join(self.results_folder, filename)

    def __load_input_file(self, filename: str) -> CSVFile:
        """
        Load input CSV file as list of rows
        """
        csv_file_content = CSVFile(headers=[], rows=[])
        first_row = True
        filepath = self.__join_csv_path(filename)
        with open(filepath) as csv_file:
            csv_reader = csv.reader(csv_file)
            for row in csv_reader:
                if first_row:
                    first_row = False
                    csv_file_content.headers = row
                    continue
                row_template = []
                for r_index in range(len(row)):
                    csv_element = CSVElement(name=csv_file_content.headers[r_index], value=row[r_index])
                    row_template.append(csv_element)
                csv_file_content.rows.append(row_template)
        return csv_file_content

    def __load_schema_file(self, filename: str) -> Template:
        filepath: str = self.__join_rules_path(filename)
        with open(filepath, "r") as y:
            json_content = yaml.load(y, Loader=yaml.FullLoader)
        result_name = json_content.get("result_name")
        template_list = json_content.get("template")
        template = Template(result_name=result_name, template=[])
        for template_list_element in template_list:
            element = TemplateElement(
                name=template_list_element.get("name"),
                type=template_list_element.get("type"),
                null_value=template_list_element.get("null_value"),
                special_cases=template_list_element.get("special_cases", [])
            )
            template.template.append(element)

        return template

    @staticmethod
    def __replace_quotations(value: str) -> str:
        result = value.replace('"', '')
        result = result.replace('""', '')
        result = result.replace('"""', '')
        return result

    def __create_result_file(self, input_file: CSVFile, template_file: Template, output_filename: str):
        result_file = ""
        template_list = template_file.template

        for template in template_list:
            if template.type == "decimal":
                result_file += "from decimal import Decimal\n"
                break

        for template in template_list:
            if template.type == "datetime":
                result_file += "import datetime\n"
                break

        result_file += "\n\n"
        result_file += f"{template_file.result_name} = [\n"
        result_file += "    [\n"

        for csv_row in tqdm(input_file.rows):
            result_file += "        {\n"

            for csv_row_element in csv_row:
                template_element = None
                for template in template_list:
                    if csv_row_element.name == template.name:
                        template_element = template
                        break

                if template_element is None:
                    continue

                if template_element.type == self.settings.tokens.DATATYPE_STRING:
                    if csv_row_element.value is None or csv_row_element.value == self.settings.tokens.NULL or csv_row_element.value == self.settings.tokens.EMPTY:
                        result_file += f'            "{csv_row_element.name}": {template_element.null_value},\n'
                    else:
                        if template_element.special_cases:
                            if self.settings.tokens.CAST_RUS_TO_ENG_SYMBOLS in template_element.special_cases:
                                if match_rus(csv_row_element.value):
                                    csv_row_element.value = cast_rus_to_eng_symbols(csv_row_element.value)
                        csv_row_element.value = self.__replace_quotations(csv_row_element.value)
                        result_file += f'            "{csv_row_element.name}": "{csv_row_element.value}",\n'
                if template_element.type == self.settings.tokens.DATATYPE_NUMBER or template_element.type == self.settings.tokens.DATATYPE_INT:
                    result_file += f'            "{csv_row_element.name}": {csv_row_element.value},\n'
                if template_element.type == self.settings.tokens.DATATYPE_DATETIME:
                    if csv_row_element.value == "" or csv_row_element.value is None or csv_row_element.value == self.settings.tokens.NULL or csv_row_element.value == self.settings.tokens.EMPTY_DASH:
                        result_file += f'            "{csv_row_element.name}": {template_element.null_value},\n'
                        continue
                    split = csv_row_element.value.split("-")
                    value = f"{int(split[0])}, {int(split[1])}, {int(split[2])}"
                    result_file += f'            "{csv_row_element.name}": datetime.date({value}),\n'
                if template_element.type == self.settings.tokens.DATATYPE_DECIMAL:
                    if csv_row_element.value is None or csv_row_element.value == "":
                        result_file += f'            "{csv_row_element.name}": {template_element.null_value},\n'
                    else:
                        result_file += f'            "{csv_row_element.name}": Decimal("{csv_row_element.value}"),\n'

            result_file += "        },\n"

        result_file += "    ]\n]"

        filepath = self.__join_results_path(output_filename)
        with open(filepath, "w") as f:
            f.write(result_file)
