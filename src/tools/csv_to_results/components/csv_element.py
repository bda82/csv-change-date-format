from dataclasses import dataclass
from typing import Any


@dataclass
class CSVElement:
    name: str
    value: Any
