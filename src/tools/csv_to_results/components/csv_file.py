from dataclasses import dataclass

from src.tools.csv_to_results.components.csv_element import CSVElement


@dataclass
class CSVFile:
    headers: list[str]
    rows: list[list[CSVElement]]
