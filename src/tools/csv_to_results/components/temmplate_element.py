from dataclasses import dataclass
from typing import Any


@dataclass
class TemplateElement:
    name: str
    type: str
    null_value: Any
    special_cases: list
