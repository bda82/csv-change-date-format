from dataclasses import dataclass

from src.tools.csv_to_results.components.temmplate_element import TemplateElement


@dataclass
class Template:
    result_name: str
    template: list[TemplateElement]
