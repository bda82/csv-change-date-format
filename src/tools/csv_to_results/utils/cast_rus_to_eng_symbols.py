import re
from typing import Any


def match_rus(text):
    return bool(re.search('[а-яА-Я]', text))


def cast_rus_to_eng_symbols(text: str | None, default_null: Any = None) -> Any:
    if text is None:
        return default_null
    alphabet = ['Ь', 'ь', 'Ъ', 'ъ', 'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё',
                'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о',
                'П', 'п', 'Р', 'р', 'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч',
                'Ш', 'ш', 'Щ', 'щ', 'Ы', 'ы', 'Э', 'э', 'Ю', 'ю', 'Я', 'я']
    transl = {'Ь': '', 'ь': '', 'ъ': '', 'Ъ': '', 'А': 'A', 'а': 'a', 'Б': 'B', 'б': 'b', 'В': 'V',
              'в': 'v', 'Г': 'G', 'г': 'g', 'Д': 'D', 'д': 'd', 'Е': 'E', 'е': 'e', 'Ё': 'JO',
              'ё': 'jo', 'Ж': 'ZH', 'ж': 'zh', 'З': 'Z', 'з': 'z', 'И': 'I', 'и': 'i', 'Й': 'J',
              'й': 'j', 'К': 'K', 'к': 'k', 'Л': 'L', 'л': 'l', 'М': 'M', 'м': 'm', 'Н': 'N',
              'н': 'n', 'О': 'O', 'о': 'o', 'П': 'P', 'п': 'p', 'Р': 'R', 'р': 'r', 'С': 'S',
              'с': 's', 'Т': 'T', 'т': 't', 'У': 'U', 'у': 'u', 'Ф': 'F', 'ф': 'f', 'Х': 'H',
              'х': 'h', 'Ц': 'С', 'ц': 'c', 'Ч': 'CH', 'ч': 'ch', 'Ш': 'SH', 'ш': 'sh',
              'Щ': 'SHH', 'щ': 'shh', 'Ы': 'Y', 'ы': 'y', 'Э': 'Je', 'э': 'je', 'Ю': 'Ju',
              'ю': 'ju', 'Я': 'Ya', 'я': 'ya'}
    result = ""
    for t in text:
        for j in t:
            if j in alphabet:
                temp = transl[j]
                result = result + temp
            else:
                temp = j
                result = result + temp
    return result
