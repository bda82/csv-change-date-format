import os
import csv
from datetime import datetime

import openpyxl
import yaml
from tqdm import tqdm

from src.tools.base_function import BaseFunction
from src.config import Config

from tools.csv_to_results.utils.cast_rus_to_eng_symbols import cast_rus_to_eng_symbols

from src.tools.excel_csv_export.components.rule import Rule
from src.tools.excel_csv_export.components.rules import Rules
from src.tools.excel_csv_export.components.excel_file import ExcelFile
from src.tools.excel_csv_export.components.cell import Cell


class ExcelCSVExport(BaseFunction):

    def run(self, config: Config):
        """
        Example call:

        python dataflow.py -f excel_csv_export -s zs01_03_excelcsv_schema.yaml -i etalon_zs01_03.xlsx -o zs01_03.csv

        Example YAML schema:

        headers: true
        delimiter: ,
        last_column: 9
        rules:
          -
            column: period
            datatype: str
            null_value: null
            tokens:
              - SPECIAL_PROCESS_AS_PERIOD
          -
            column: name NREC(KAU1)
            datatype: str
            null_value: null
            tokens:
              - REMOVE_COMMA_INSIDE_FIELD
              - REMOVE_DOUBLE_QUOTATION_MARKS
        """
        rules: Rules = self.__load_rules_file(config.schema)
        excel_file_content: ExcelFile = self.__load_excel_file(config.input, rules)
        self.__save_csv_file(excel_file_content, config.output, rules)

    def __join_rules_path(self, filename: str):
        return os.path.join(self.schemas_folder, filename)

    def __join_csv_path(self, filename: str):
        return os.path.join(self.csv_folder, filename)

    def __join_excel_path(self, filename: str):
        return os.path.join(self.excel_folder, filename)

    def __load_rules_file(self, filename: str) -> Rules:
        filepath: str = self.__join_rules_path(filename)
        with open(filepath, "r") as y:
            json_content = yaml.load(y, Loader=yaml.FullLoader)
        headers = json_content.get("headers", True)
        delimiter = json_content.get("delimiter")
        rules = json_content.get("rules", [])
        last_column = json_content.get("last_column", 0)
        last_row = json_content.get("last_row", 0)

        rules_dc = []
        for rule in rules:
            column = rule.get("column")
            datatype = rule.get("datatype", "str")
            null_value = rule.get("null_value")
            tokens = rule.get("tokens", [])
            rule_dc = Rule(
                column=column,
                datatype=datatype,
                null_value=null_value,
                tokens=tokens
            )
            rules_dc.append(rule_dc)

        return Rules(
            headers=headers,
            delimiter=delimiter,
            rules=rules_dc,
            last_column=last_column,
            last_row=last_row
        )

    @staticmethod
    def __define_last_not_null_row(sheet) -> int:
        last_row = 0
        for row in tqdm(range(1, sheet.max_row + 1)):
            cells = []
            for column in range(1, sheet.max_column + 1):
                cell = sheet.cell(row=row, column=column)
                if cell:
                    cells.append(True)
            if not any(cells):
                return last_row
            last_row += 1
        return last_row

    @staticmethod
    def __define_last_not_null_column(sheet) -> int:
        columns = 0
        for column in tqdm(range(1, sheet.max_column + 1)):
            cell = sheet.cell(row=1, column=column)
            if not cell:
                break
            else:
                columns += 1
        return columns

    def __load_excel_file(self, filename: str, rules: Rules) -> ExcelFile:
        filepath: str = self.__join_excel_path(filename)
        workbook = openpyxl.load_workbook(filepath)
        sheet = workbook.active

        first_row = True

        headers = []
        rows = []

        if rules.last_row == 0:
            print("Define last row number from Sheet...")
            last_row = self.__define_last_not_null_row(sheet)
        else:
            last_row = rules.last_row
            print("Apply last row number from rules file...")

        if rules.last_column == 0:
            print("Define last column number from Sheet...")
            last_column = self.__define_last_not_null_column(sheet)
        else:
            print("Apply last column number from Sheet...")
            last_column = rules.last_column

        print("Rows quantity: ", last_row)
        print("Columns quantity: ", last_column)

        for row in range(1, last_row + 1):
            row_n: list[Cell] = []
            for column in range(1, last_column + 1):
                cell = sheet.cell(row=row, column=column)
                if first_row:
                    headers.append(cell.value)
                else:
                    cell_dc = Cell(column=headers[column - 1], value=cell.value)
                    row_n.append(cell_dc)
            if first_row:
                first_row = False
            else:
                rows.append(row_n)

        return ExcelFile(headers=headers, rows=rows)

    @staticmethod
    def __datetime_to_csv_format(dt: datetime):
        year = str(dt.year)
        month = str(dt.month) if dt.month >= 10 else f"0{dt.month}"
        day = str(dt.day) if dt.day >= 10 else f"0{dt.day}"
        return f"{year}-{month}-{day}"

    @staticmethod
    def __find_rule(column: str, rules: Rules) -> Rule | None:
        for rule in rules.rules:
            if column == rule.column:
                return rule

    def __convert_row_into_csv(self, row: list[Cell], rules: Rules) -> list[Cell]:
        converted: list[Cell] = []
        for element in row:
            converted_element = element.value
            if converted_element is not None:
                rule = self.__find_rule(element.column, rules)
                if rule:
                    if self.settings.tokens.SAVE_AS_IS in rule.tokens:
                        pass
                    else:
                        special_tokens = []
                        for token in rule.tokens:
                            if token in self.settings.tokens.SPECIAL_RULES:
                                special_tokens.append(token)
                        if special_tokens:
                            for special_token in special_tokens:
                                pass
                        else:
                            if rule.datatype == self.settings.tokens.DATATYPE_STRING:
                                for token in rule.tokens:
                                    if token == self.settings.tokens.CAST_RUS_TO_ENG_SYMBOLS:
                                        converted_element = cast_rus_to_eng_symbols(converted_element, rule.null_value)
                                    if token == self.settings.tokens.TRIM_WHITESPACES_AFTER:
                                        converted_element = converted_element.rstrip()
                                    if token == self.settings.tokens.TRIM_WHITESPACES_BEFORE:
                                        converted_element = converted_element.lstrip()
                                    if token == self.settings.tokens.REMOVE_COMMA_INSIDE_FIELD:
                                        converted_element = converted_element.replace(",", "")
                                    if token == self.settings.tokens.REMOVE_DOUBLE_QUOTATION_MARKS:
                                        converted_element = converted_element.replace('"', "")
                                    if token == self.settings.tokens.REMOVE_NEW_LINE_SYMBOLS:
                                        converted_element = converted_element.replace("\n", "")
                            if rule.datatype == self.settings.tokens.DATATYPE_DATETIME:
                                if isinstance(converted_element, datetime):
                                    converted_element = self.__datetime_to_csv_format(converted_element)
                            if rule.datatype == self.settings.tokens.DATATYPE_DECIMAL:
                                converted_element = int(converted_element)
                            if rule.datatype == self.settings.tokens.DATATYPE_NUMBER:
                                if self.settings.tokens.CLIPPING_NUMBERS_AFTER_DECIMAL_POINT in rule.tokens:
                                    fmt = "{:." + str(self.settings.tokens.CLIPPING_NUMBERS_AFTER_DECIMAL_POINT_COUNT) + "f}"
                                    converted_element = float(fmt.format(converted_element))
            converted.append(converted_element)
        return converted

    def __save_csv_file(self, excel_content: ExcelFile, filename: str, rules: Rules):
        filepath: str = self.__join_csv_path(filename)
        cache = []
        if rules.headers:
            cache.append(excel_content.headers)
        for row in tqdm(excel_content.rows):
            cache.append(self.__convert_row_into_csv(row, rules))
        with open(filepath, "w") as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=rules.delimiter)
            for row in cache:
                csv_writer.writerow(row)
