from dataclasses import dataclass

from src.tools.excel_csv_export.components.rule import Rule


@dataclass
class Rules:
    rules: list[Rule]
    delimiter: str
    headers: bool
    last_column: int = 0
    last_row: int = 0
