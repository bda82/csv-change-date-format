from dataclasses import dataclass
from typing import Any


@dataclass
class CSVFile:
    headers: list[str]
    rows: list[Any]
