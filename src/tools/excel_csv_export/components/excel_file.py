from dataclasses import dataclass

from src.tools.excel_csv_export.components.cell import Cell


@dataclass
class ExcelFile:
    headers: list[str]
    rows: list[list[Cell]]
