from dataclasses import dataclass
from typing import Any


@dataclass
class Cell:
    column: str
    value: Any
