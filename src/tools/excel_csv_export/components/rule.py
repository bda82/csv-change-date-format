from dataclasses import dataclass
from typing import Any


@dataclass
class Rule:
    column: str
    datatype: str
    tokens: list[str]
    null_value: Any
