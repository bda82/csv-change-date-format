import os
from abc import ABC, abstractmethod

from src.config import Config
from src.settings import Settings


class BaseFunction(ABC):
    settings: Settings = Settings()
    base_dir: str = os.getcwd()

    def __init__(self):
        self.csv_folder = os.path.join(self.base_dir, self.settings.folders.csv)
        self.excel_folder = os.path.join(self.base_dir, self.settings.folders.excel)
        self.schemas_folder = os.path.join(self.base_dir, self.settings.folders.schemas)
        self.scenarios_folder = os.path.join(self.base_dir, self.settings.folders.scenarios)
        self.results_folder = os.path.join(self.base_dir, self.settings.folders.results)

    @abstractmethod
    def run(self, config: Config):
        raise NotImplementedError
