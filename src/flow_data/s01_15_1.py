from sqlalchemy import Column, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_15_1 = Table(
    "s01_15_1",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("account", String),
    Column("name_indicator", String),
    Column("repair_int", Numeric(29, 14)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
