from sqlalchemy import Column, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_48 = Table(
    "s01_48",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("account", String),
    Column("subaccount", String),
    Column("nrec(KAU1)", String),
    Column("name nrec(KAU1)", String),
    Column("corr_acc", String),
    Column("turn_dt", Numeric(15, 2)),
    Column("turn_kt", Numeric(15, 2)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
