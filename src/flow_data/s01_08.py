from sqlalchemy import Column, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_08 = Table(
    "s01_08",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("month", String),
    Column("subaccount", String),
    Column("bal_in", Numeric(15, 2)),
    Column("bal_in_amount", Numeric(15, 4)),
    Column("turn_dt", Numeric(15, 2)),
    Column("turn_dt_amount", Numeric(15, 4)),
    Column("turn_kt", Numeric(15, 2)),
    Column("turn_kt_amount", Numeric(15, 4)),
    Column("bal_ref", Numeric(15, 2)),
    Column("bal_ref_amount", Numeric(15, 4)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
