from sqlalchemy import Column, Date, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_66 = Table(
    "s01_66",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("order", String),
    Column("inv_num", String),
    Column("object_name", String),
    Column("entry_date", Date),
    Column("cost_acc", String),
    Column("note", String),
    Column("spi", Numeric(4)),
    Column("initial_cost", Numeric(17, 2)),
    Column("modern_sum", Numeric(17, 2)),
    Column("recon_sum", Numeric(17, 2)),
    Column("oper_date", Date),
    Column("cor_period", String),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
