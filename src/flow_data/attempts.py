from sqlalchemy import TIMESTAMP, Column, Integer, String, Table
from old.tables.tables_defnitions.metadata import metadata

attempts = Table(
    "attempts",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("number", Integer),
    Column("be", String),
    Column("year", String),
    Column("period", String),
    Column("scenario_id", Integer),
    Column("registry_name", String),
    Column("execution_date", TIMESTAMP),
)
