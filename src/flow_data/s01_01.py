from sqlalchemy import Column, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata


s01_01 = Table(
    "s01_01",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("month", String),
    Column("bal_in_debet_amount", Numeric(19, 4)),
    Column("bal_in_debet", Numeric(17, 2)),
    Column("turn_dt_bu", Numeric(17, 2)),
    Column("turn_dt_nu", Numeric(17, 2)),
    Column("turn_dt_amount", Numeric(19, 4)),
    Column("turn_kt_amount", Numeric(19, 4)),
    Column("turn_kt", Numeric(17, 2)),
    Column("bal_ref_debet", Numeric(17, 2)),
    Column("bal_ref_debet_amount", Numeric(19, 4)),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", String),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", String),
)
