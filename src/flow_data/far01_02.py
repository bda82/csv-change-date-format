from sqlalchemy import Column, Date, ForeignKey, Integer, Numeric, String, Table
from old.tables.table_defs import metadata


far01_02 = Table(
    "far01_02",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("attempt_id", Integer, ForeignKey("attempts.id")),
    Column("order", String),
    Column("inv_num", String),
    Column("name_os", String),
    Column("amort_group_os", Numeric(2)),
    Column("amort_prem_percent", Numeric(3)),
    Column("spi", Numeric(4)),
    Column("entry_date", Date),
    Column("initial_cost", Numeric(17, 2)),
    Column("modern_cost", Numeric(17, 2)),
    Column("modern_sum", Numeric(17, 2)),
    Column("modern_date", Date),
    Column("cor_period", String),
    Column("cost_acc", String),
    Column("analytics", String),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
