from sqlalchemy import Column, Date, Integer, Numeric, String, Table
from old.tables.tables_defnitions.metadata import metadata

s01_69 = Table(
    "s01_69",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("order", String),
    Column("inv_num", String),
    Column("object_name", String),
    Column("entry_date", Date),
    Column("spi", Numeric(4)),
    Column("initial_cost", Numeric(17, 2)),
    Column("summ_epreciation", Numeric(17, 2)),
    Column("summ_epreciation_date", Numeric(17, 2)),
    Column("residual_value", Numeric(17, 2)),
    Column("account", String),
    Column("taxform", String),
    Column("taxpayer", String),
    Column("ifns", String),
    Column("kpp", String),
    Column("fiscal_year", Numeric(4)),
    Column("period", String),
    Column("oktmo", String),
    Column("corr_num", Numeric(3)),
)
