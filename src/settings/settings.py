from dataclasses import dataclass, field

from src.settings.tokens import Tokens
from src.settings.functions import Functions
from src.settings.folders import Folders


@dataclass
class Settings:
    tokens: Tokens = field(default_factory=Tokens)
    folders: Folders = field(default_factory=Folders)
    functions: Functions = field(default_factory=Functions)

