from dataclasses import dataclass


@dataclass
class Folders:
    csv: str = "csv"
    excel: str = "excel"
    schemas: str = "schemas"
    scenarios: str = "scenarios"
    results: str = "results"
