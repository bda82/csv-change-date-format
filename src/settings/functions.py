from dataclasses import dataclass


@dataclass
class Functions:
    csv_to_results: str = "csv_to_results"
    excel_csv_export: str = "excel_csv_export"
    reformat_date: str = "reformat_csv_date"
    db_flow: str = "db_flow"
    FILL_DATABASE = "FILL_DATABASE"  # первый сценарий для заполнения базы
    EXECUTE_SQL = "EXECUTE_SQL"  # второй сценарий - выполнение SQL
