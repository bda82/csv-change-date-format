import argparse
from src.config import Config

from src.settings.settings import Settings

from src.tools.base_function import BaseFunction
from src.tools.reformat_csv_date import ReformatDate
from src.tools.csv_to_results import CSVToResults
from src.tools.excel_csv_export import ExcelCSVExport
from src.tools.db_flow import DBFlow

settings: Settings = Settings()


def dispatch_function(config_: Config):
    function: BaseFunction | None = None

    if config_.function == settings.functions.csv_to_results:
        function = CSVToResults()
    elif config_.function == settings.functions.reformat_date:
        function = ReformatDate()
    elif config_.function == settings.functions.excel_csv_export:
        function = ExcelCSVExport()
    elif config_.function == settings.functions.db_flow:
        function = DBFlow()

    if function is not None:
        return function.run(config_)
    else:
        raise ValueError("Cant define function name")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Dataflow argument parser",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("-f", "--function", type=str, help="operation type")
    parser.add_argument("-s", "--schema", type=str, help="schema file")
    parser.add_argument("-i", "--input", type=str, help="source file")
    parser.add_argument("-o", "--output", type=str, help="destination file")

    args = parser.parse_args()

    config = Config(**vars(args))

    dispatch_function(config_=config)
